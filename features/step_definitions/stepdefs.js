const assert = require('assert');
const { Given, When, Then } = require('cucumber');
const print_the_bill = require('../../statement');

Given('el listado de la facturación de espectáculos', function (espectaculos) {
   this.invoice = JSON.parse(espectaculos);
});

Given('la lista de obras', function (obras) {
   this.play = JSON.parse(obras);
});

// imprimir en texto plano en dólares
When('mando a imprimir el borderau', function () {
   this.actualAnswer = print_the_bill.render(this.invoice, this.play, "plainText", "USD");
});

Then('debería imprimir el borderau', function (expectedAnswer) {
   assert.equal(this.actualAnswer.trim(), expectedAnswer.trim());;
});

// imprimir en texto HTML en dólares
When('mando a imprimir el borderau en HTML', function () {
   this.actualAnswer = print_the_bill.render(this.invoice,this.play, "HTML", "USD");
});

Then('debería imprimir el borderau en HTML', function (expectedAnswer) {
   assert.equal(this.actualAnswer.trim(), expectedAnswer.trim());;
});

// imprimir en texto plano en pesos argentinos
When('mando a imprimir el borderau en ARS', function () {
   this.actualAnswer = print_the_bill.render(this.invoice,this.play, "plainText", "ARS");
});

Then('debería imprimir el borderau en ARS', function (expectedAnswer) {
   assert.equal(this.actualAnswer.trim(), expectedAnswer.trim());;
});

/// imprimir en HTML en pesos argentinos
When('mando a imprimir el borderau en HTML en ARS', function () {
   this.actualAnswer = print_the_bill.render(this.invoice,this.play, "HTML", "ARS");
});

Then('debería imprimir el borderau en HTML en ARS', function (expectedAnswer) {
   assert.equal(this.actualAnswer.trim(), expectedAnswer.trim());;
});

/// imprimir en texto plano en rublos
When('mando a imprimir el borderau en RUB', function () {
   this.actualAnswer = print_the_bill.render(this.invoice,this.play, "plainText", "RUB");
});

Then('debería imprimir el borderau en RUB', function (expectedAnswer) {
   assert.equal(this.actualAnswer.trim(), expectedAnswer.trim());;
});

