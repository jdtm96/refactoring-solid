const createStatementData = require('./createStatementData');

exports.statement = (invoice, plays) => render(createStatementData(invoice, plays));

// el que llama main
 function render(statementData){
   return createRender(statementData, "plainText", "USD").render;
 }


// el que llama stepdefs
 exports.render = function (invoice, plays, textType, currencyType) {
   return createRender(createStatementData(invoice, plays), textType, currencyType).render;
 }
 
 class Render {
   constructor(statementData, currencyType){
      this.statementData =  statementData;
      this.currency = createCurrency(currencyType);
   }

   get render() {
      throw new Error(`render not supported in Render`);
   }
 }

 function createRender(statementData, textType, currencyType) {
   switch(textType){
      case "plainText": return new RenderPlainText(statementData, currencyType);
      case "HTML": return new RenderHTML(statementData, currencyType);
      default:
         throw new Error(`unknown text type: ${textType}`);
   }
   
}

 class RenderPlainText extends Render {
    get render() {
      let result = `Statement for ${this.statementData.customer}\n`;
 
      for (let perf of this.statementData.performances) {
         result += `  ${perf.play.name}: ${this.currency.format(perf.amount)} (${perf.audience} seats)\n`;
      }
   
      result += `Amount owed is ${this.currency.format(this.statementData.totalAmount)}\n`;
      result += `You earned ${this.statementData.totalVolumeCredits} credits\n`;
      return result;
    }
 }

 class RenderHTML extends Render {
    get render() { 
      let result = `<h1>Statement for ${this.statementData.customer}</h1>\n`;
      result += "<table>\n";
      result += "<tr><th>play</th><th>seats</th><th>cost</th></tr>\n";
      for (let perf of this.statementData.performances) {
        result += `<tr><td>${perf.play.name}</td><td>${perf.audience}</td>`;
        result += `<td>${this.currency.format(perf.amount)}</td></tr>\n`;
      }
      result += "</table>\n";
      result += `<p>Amount owed is <em>${this.currency.format(this.statementData.totalAmount)}</em></p>\n`;
      result += `<p>You earned <em>${this.statementData.totalVolumeCredits}</em> credits</p>\n`;
      return result;
    }
 }

 class Currency {
    
   format(){
      throw new Error(`format not supported in Currency`);
    }
 }

 function createCurrency(currencyType) {
   switch(currencyType){
      case "USD": return new Usd();
      case "ARS": return new Ars();
      case "RUB": return new Rub();
      default:
         throw new Error(`unknown currency type: ${currencyType}`);
   }
   
}

 class Usd extends Currency{

   format(aNumber){
      return new Intl.NumberFormat("en-US",
       {
          style: "currency", currency: "USD",
          minimumFractionDigits: 2
       }).format(aNumber / 100)
   }
 }

 class Ars extends Currency{

   format(aNumber){
       return new Intl.NumberFormat("es-ARG",
      {
         style: "currency", currency: "ARS",
         minimumFractionDigits: 2
      }).format(aNumber / 100)
   }
}

class Rub extends Currency{

   format(aNumber){
      return new Intl.NumberFormat("ru-RU",
       {
          style: "currency", currency: "RUB",
          minimumFractionDigits: 2
       }).format(aNumber / 100)
   }
 }